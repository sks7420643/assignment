package com.sarwar.assignment.dto;

import lombok.Data;

import java.util.List;

@Data
public class Order {
    private int orderId;
    private String discount;
    private List<Item> items;
}