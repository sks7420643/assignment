package com.sarwar.assignment.dto;

import lombok.Data;

@Data
public class Discount {
    private String key;
    private double value;
    private String stacks;
}